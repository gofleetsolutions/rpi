# Copyright 2018-2019 GO FLEET SOLUTIONS S.R.L. See LICENSE for details.

CDIR := "$(shell pwd)"
BUILD_DIR := "$(CDIR)/build"
YOCTO_DIR := "$(CDIR)/yocto"

DOCKER_IMAGE_RPI := registry.gitlab.com/gofleetsolutions/rpi:latest

all: build-rpi

build-rpi:
		@ if [ ! -d "$(BUILD_DIR)" ]; then \
			cp -rv "$(BUILD_DIR)"-template "$(BUILD_DIR)"; \
		fi

		@ docker run -t \
			--rm \
			--volume="$(YOCTO_DIR)":/home/user/yocto \
			--volume="$(BUILD_DIR)":/home/user/build:rw \
			--name rpi \
			"$(DOCKER_IMAGE_RPI)" \
			bash -c 'source /home/user/yocto/poky/oe-init-build-env /home/user/build && bitbake rpi-custom-image'

run:
		@ docker run -it \
			--rm \
			--volume="$(YOCTO_DIR)":/home/user/yocto \
			--volume="$(BUILD_DIR)":/home/user/build:rw \
			--name rpi \
			"$(DOCKER_IMAGE_RPI)" \
			bash

clean:
		@ rm -rf "$(BUILD_DIR)"
