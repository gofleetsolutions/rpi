# Append the wpa-supplicant recipe with custom configuration

FILESEXTRAPATHS_prepend := "${THISDIR}/${PN}:"
SRC_URI += "file://htc_9271-1.4.0.fw"

FILES_${PN} += "${base_libdir}/firmware/ath9k_htc/htc_9271-1.4.0.fw"

do_install_append () {

				install -d ${D}${sysconfdir}

        if [ -e "${WPA_CONFIG}" ]; then
				   install -m 600 "${WPA_CONFIG}" "${D}${sysconfdir}/wpa_supplicant.conf"
        fi

				install -d ${D}${base_libdir}/firmware/ath9k_htc
				install -m 600 ${WORKDIR}/htc_9271-1.4.0.fw ${D}${base_libdir}/firmware/ath9k_htc/htc_9271-1.4.0.fw
}
