DESCRIPTION = "Python library for the NMEA 0183 protcol"
HOMEPAGE = "https://github.com/Knio/pynmea2"
SECTION = "devel/python"
LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://setup.py;md5=17d240c9f8c97f5b165cfd5cf8822038"

SRC_URI[md5sum] = "b4ccf56ea1e5521133bb7880adff6dc7"
SRC_URI[sha256sum] = "f1509fe9a424f2ff0547a0b8475f807a587e3482eb6aed6b873c3df590edbca0"

inherit pypi
