SUMMARY = "A small image based on core-image-minimal for RPi target."

LICENSE = "MIT"

inherit core-image

IMAGE_FEATURES += "ssh-server-dropbear hwcodecs"

IMAGE_INSTALL_append = " \
  ${CORE_IMAGE_EXTRA_INSTALL} \
  packagegroup-core-boot \
  wpa-supplicant \
  iw \
  opencv \
  libopencv-core \
  libopencv-imgproc \
  python3 \
  python3-pip \
  python3-picamera \
  python3-fscanner \
  python3-gpsbroker \
  python3-mqbroker \
  python3-opencv \
  python3-wscanner \
  tcpdump \
"

DISTRO_FEATURES_append = " pam egl gles"
DISTRO_FEATURES_remove = " x11 wayland vulkan"