DESCRIPTION = "Wireless device scanner"
HOMEPAGE = "https://gitlab.com/gofleetsolutions/wscanner"
SECTION = "devel/python"
LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://LICENSE;md5=65c72d7974b747e2762bf57ccb605102"

PV = "0.1+git${SRCPV}"
SRCREV = "${AUTOREV}"
SRC_URI = "git://gitlab.com/gofleetsolutions/wscanner.git;protocol=https;branch=master \
           file://wscanner.init \
           file://wscanner.service \
"

S = "${WORKDIR}/git"

inherit setuptools3 systemd update-rc.d

RDEPENDS_${PN}_class-target += " \
    ${PYTHON_PN}-attrs \
    ${PYTHON_PN}-scapy \
    ${PYTHON_PN}-pyzmq \
"

FILES_${PN} += "${sysconfdir}/init.d \
                ${systemd_unitdir}/system/wscanner.service \
"

SYSTEMD_SERVICE_${PN} = "wscanner.service"

INITSCRIPT_NAME = "wscanner"
INITSCRIPT_PARAMS = "defaults 90 10"

do_configure_prepend() {
    sed 's/.*scapy.*//' -i ${WORKDIR}/git/setup.py
}

do_install_append() {
  install -d ${D}${sysconfdir}/init.d/
  install -m 0755 ${WORKDIR}/wscanner.init ${D}${sysconfdir}/init.d/wscanner

  if ${@bb.utils.contains('DISTRO_FEATURES','systemd','true','false',d)}; then
    install -d ${D}${systemd_unitdir}/system/
    install -m 0644 ${WORKDIR}/wscanner.service ${D}${systemd_unitdir}/system/wscanner.service
  fi
}
