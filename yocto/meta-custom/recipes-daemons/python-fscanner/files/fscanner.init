#!/bin/sh
#
# fscanner		Start script for the fscanner daemon
#
# chkconfig:	- 90 10
# description:	Face & body detection and counting
# processname:	fscanner
#
### BEGIN INIT INFO
# Provides: fscanner
# Required-Start: $local_fs $remote_fs $network $named
# Required-Stop: $local_fs $remote_fs $network
# Should-Start: $syslog
# Should-Stop: $network $syslog
# Default-Start: 2 3 4 5
# Default-Stop: 0 1 6
# Short-Description: Start and stop fscanner daemon
# Description:	Face & body detection and counting
### END INIT INFO

# Source function library.
. /etc/init.d/functions

PROG='fscanner'
DAEMON="/usr/bin/${PROG}"
OPTIONS="--addr ipc:///tmp/scan.bfs --topic SCAN.BFS --quiet --period 1"
PIDFILE="/var/run/${PROG}.pid"

start() {
  [ -x "${DAEMON}" ] || exit 1

  echo "Starting ${PROG}"
  modprobe bcm2835-v4l2
  if start-stop-daemon --start --background --make-pidfile --pidfile ${PIDFILE} \
                       --exec ${DAEMON} -- ${OPTIONS}; then
    exit 0
  else
    exit 1
  fi
}

stop() {
  echo "Stopping ${PROG}"
  if start-stop-daemon --stop --retry 30 --pidfile ${PIDFILE}; then
    rm -f ${PIDFILE}
    exit 0
  else
    exit 1
  fi
}

case "$1" in
  start)
    start
    ;;
  stop)
    stop
    ;;
  restart)
    stop
    start
    ;;
  status)
    status ${DAEMON} && exit 0 || exit $?
    ;;
  *)
    echo $"Usage: ${PROG} {start|stop|status|restart}"
    exit 1
esac

exit 0
