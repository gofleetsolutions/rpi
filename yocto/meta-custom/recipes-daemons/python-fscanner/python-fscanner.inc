DESCRIPTION = "Face & body detection and counting"
HOMEPAGE = "https://gitlab.com/gofleetsolutions/fscanner"
SECTION = "devel/python"
LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://LICENSE;md5=65c72d7974b747e2762bf57ccb605102"

PV = "0.1+git${SRCPV}"
SRCREV = "${AUTOREV}"
SRC_URI = "git://gitlab.com/gofleetsolutions/fscanner.git;protocol=https;branch=master \
           file://fscanner.init \
           file://fscanner.service \
"

S = "${WORKDIR}/git"

inherit setuptools3 systemd update-rc.d

RDEPENDS_${PN}_class-target += " \
    ${PYTHON_PN}-attrs \
    ${PYTHON_PN}-opencv \
    ${PYTHON_PN}-pyzmq \
"

FILES_${PN} += "${sysconfdir}/init.d \
                ${systemd_unitdir}/system/fscanner.service \
"

SYSTEMD_SERVICE_${PN} = "fscanner.service"

INITSCRIPT_NAME = "fscanner"
INITSCRIPT_PARAMS = "defaults 90 10"

do_configure_prepend() {
  sed 's/.*opencv-python.*//' -i ${WORKDIR}/git/setup.py
}

do_install_append() {
  install -d ${D}${sysconfdir}/init.d/
  install -m 0755 ${WORKDIR}/fscanner.init ${D}${sysconfdir}/init.d/fscanner

  if ${@bb.utils.contains('DISTRO_FEATURES','systemd','true','false',d)}; then
    install -d ${D}${systemd_unitdir}/system/
    install -m 0644 ${WORKDIR}/fscanner.service ${D}${systemd_unitdir}/system/fscanner.service
  fi
}
