DESCRIPTION = "Gps broker"
HOMEPAGE = "https://gitlab.com/gofleetsolutions/gpsbroker"
LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://LICENSE;md5=65c72d7974b747e2762bf57ccb605102"

PV = "0.1+git${SRCPV}"
SRCREV = "${AUTOREV}"
SRC_URI = "git://gitlab.com/gofleetsolutions/gpsbroker.git;protocol=https;branch=master \
           file://gpsbroker.init \
           file://gpsbroker.service \
"

S = "${WORKDIR}/git"

inherit setuptools3 systemd update-rc.d

RDEPENDS_${PN}_class-target += " \
    ${PYTHON_PN}-attrs \
    ${PYTHON_PN}-pynmea2 \
    ${PYTHON_PN}-pyzmq \
"

FILES_${PN} += "${sysconfdir}/init.d \
                ${systemd_unitdir}/system/gpsbroker.service \
"

SYSTEMD_SERVICE_${PN} = "gpsbroker.service"

INITSCRIPT_NAME = "gpsbroker"
INITSCRIPT_PARAMS = "defaults 90 10"

do_install_append() {
  install -d ${D}${sysconfdir}/init.d/
  install -m 0755 ${WORKDIR}/gpsbroker.init ${D}${sysconfdir}/init.d/gpsbroker

  if ${@bb.utils.contains('DISTRO_FEATURES','systemd','true','false',d)}; then
    install -d ${D}${systemd_unitdir}/system/
    install -m 0644 ${WORKDIR}/gpsbroker.service ${D}${systemd_unitdir}/system/gpsbroker.service
  fi
}
