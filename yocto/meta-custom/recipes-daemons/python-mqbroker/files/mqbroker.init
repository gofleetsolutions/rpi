#!/bin/sh
#
# mqbroker    Start script for the mqbroker daemon
#
# chkconfig:  - 90 10
# description:  ZeroMQ message broker
# processname:  mqbroker
#
### BEGIN INIT INFO
# Provides: mqbroker
# Required-Start: $local_fs $remote_fs $network $named
# Required-Stop: $local_fs $remote_fs $network
# Should-Start: $syslog
# Should-Stop: $network $syslog
# Default-Start: 2 3 4 5
# Default-Stop: 0 1 6
# Short-Description: Start and stop mqbroker daemon
# Description:  ZeroMQ message broker
### END INIT INFO

# Source function library.
. /etc/init.d/functions

PROG='mqbroker'
DAEMON="/usr/bin/${PROG}"
OPTIONS="--src ipc:///tmp/scan.gps --src ipc:///tmp/scan.wdev --src ipc://tmp/scan.bfs \
--dst tcp://*:55555 --topic-filter SCAN"
PIDFILE="/var/run/${PROG}.pid"

start() {
  [ -x "${DAEMON}" ] || exit 1

  echo "Starting ${PROG}"
  if start-stop-daemon --start --background --make-pidfile --pidfile ${PIDFILE} \
                       --exec ${DAEMON} -- ${OPTIONS}; then
    exit 0
  else
    exit 1
  fi
}

stop() {
  echo "Stopping ${PROG}"
  if start-stop-daemon --stop --retry 30 --pidfile ${PIDFILE}; then
    rm -f ${PIDFILE}
    exit 0
  else
    exit 1
  fi
}

case "$1" in
  start)
    start
    ;;
  stop)
    stop
    ;;
  restart)
    stop
    start
    ;;
  status)
    status ${DAEMON} && exit 0 || exit $?
    ;;
  *)
    echo $"Usage: ${PROG} {start|stop|status|restart}"
    exit 1
esac

exit 0
